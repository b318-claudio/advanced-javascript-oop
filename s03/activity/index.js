/* Activity Part 1 */
class RegularShape{
	constructor(noSides, length){
		this.noSides = noSides;
		this.length = length;

		if(this.constructor === RegularShape){
			throw new Error(
				"Object cannot be created from an abstract class RegularShape."
			);
		}

		// if(this.noSides === undefined || this.length === undefined){
		// 	throw new Error(
		// 		"Length and number of sides must be provided."
		// 	);
		// }

		if(this.getPerimeter === undefined){
			throw new Error(
				"Class must implement getPerimeter() method."
			);
		}

		if(this.getArea === undefined){
			throw new Error(
				"Class must implement getArea() method."
			);
		}
	}
}

class Square extends RegularShape{
	getPerimeter(){
		return `The perimeter of the square is ` + (this.noSides * this.length);
	}

	getArea(){
		return `The area of the square is ` + (this.length ** 2);
	}
}

let shape1 = new Square(4, 16);
console.log(shape1.getPerimeter());
console.log(shape1.getArea());



/* Activity Part 2 */
class Food{
	constructor(name, price){
		this.name = name;
		this.price = price;

		if(this.constructor === Food){
			throw new Error(
				"Object cannot be created from an abstract class Food"
			);
		}

		// if(this.name === undefined || this.price === undefined){
		// 	throw new Error(
		// 		"Name and price must be provided."
		// 	);
		// }

		if(this.getName === undefined){
			throw new Error(
				"Class must implement getName() method."
			);
		}
	}
}

class Vegetable extends Food{
	constructor(name, breed, price){
		super(name, price);
		this.breed = breed;
	}

	getName(){
		return `${this.name} is of ${this.breed} variety and is priced at ${this.price} pesos.`;
	}
}


const vegetable1 = new Vegetable("Pechay", "Native", 25);
console.log(vegetable1.getName());



/* Activity Part 3 */
class Equipment{
	constructor(equipmentType, model){
		this.equipmentType = equipmentType;
		this.model = model;

		if(this.constructor === Equipment){
			throw new Error(
				"Object cannot be created from an abstract class Equipment"
			);
		}

		// if(this.equipmentType === undefined || this.model === undefined){
		// 	throw new Error(
		// 		"Equipment Type and Model must be provided."
		// 	);
		// }

		if(this.printInfo === undefined){
			throw new Error(
				"Class must implement printInfo() method."
			);
		}
	}
}

class Bulldozer extends Equipment{
	constructor(equipmentType, model, bladeType){
		super(equipmentType, model);
		this.bladeType = bladeType;
	}

	printInfo(){
		return `Info: ${this.equipmentType}\nThe bulldozer ${this.model} has a ${this.bladeType} blade`;
	}
}

class TowerCrane extends Equipment{
	constructor(equipmentType, model, hookRadius, maxCapacity){
		super(equipmentType, model);
		this.hookRadius = hookRadius;
		this.maxCapacity = maxCapacity;
	}

	printInfo(){
		return `Info: ${this.equipmentType}\nThe tower crane ${this.model} has ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity`;
	}
}


let bulldozer1 = new Bulldozer("bulldozer", "Brute", "Shovel");
let towercrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500);
console.log(bulldozer1.printInfo());
console.log(towercrane1.printInfo());