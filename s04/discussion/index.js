/*
	Abstraction is like creating a simplified model or blueprint. It helps us focus on what's important and ignore the complicated details.
*/


// This class will serve as blueprint for Employee class
class Person{
	constructor(){
		if(this.constructor === Person){
			throw new Error(
				"Object cannot be created from an abstract class Person"
			);
		}

		if(this.getFullName === undefined){
			throw new Error(
				"Class must implement getFullName() method."
			);
		}
	}
}

class Employee extends Person{

	// Encapsulation aided by private fields (#), ensures data protection. Setters and getters provide controlled access to encapsulated data.

	// Private fields
	#firstName;
	#lastName;
	#employeeID;
	#password

	constructor(firstName, lastName, employeeID, password){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#employeeID = employeeID;
		this.#password = password;
	}

	// getter methods
	getFirstName(){
		return `First Name: ${this.#firstName}`;
	}

	getLastName(){
		return `Last Name: ${this.#lastName}`;
	}

	getEmployeeId(){
		return this.#employeeID;
	}

	getFullName(){
		return `${this.#firstName} ${this.#lastName} has employeeID of ${this.#employeeID}`;
	}

	// setter methods
	setFirstName(firstName){
		this.#firstName = firstName;
	}

	setLastName(lastName){
		this.#lastName = lastName;
	}

	setEmployeeID(employeeID){
		if(this.#password === undefined){
			throw new Error(
				"You are not authorized to use this function"
			);
		} else{
			this.#employeeID = employeeID;
		}
	}

	inputPassword(providedPassword){
		if(providedPassword === "access1001"){
			this.#password = providedPassword;
		} else{
			throw new Error(
				"Wrong password"
			)
		}
	}
}

const employeeA = new Employee("John", "Smith", "EM-001");

// direct access with the field/property firstName will return undefined because the property is private.
// console.log(employeeA.firstName);

// We could directly change the value of the property firstName becuase the property is private.
// employeeA.firstName = "David";

// console.log(employeeA.getFirstName());
// employeeA.setFirstName("David");
// console.log(employeeA.getFirstName());
// console.log(employeeA.getFullName());

// const employeeB = new Employee();
// console.log(employeeB.getFullName());
// employeeB.setFirstName("Jill");
// employeeB.setLastName("Hill");
// employeeB.setEmployeeID("EM-002");
// console.log(employeeB.getFullName());

const employeeC = new Employee();
// employeeC.password = "access1001";
employeeC.inputPassword("access1001");
employeeC.setEmployeeID("EM-003");
console.log(employeeC.getEmployeeId());