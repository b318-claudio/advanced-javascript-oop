/* Activity Part 1 */
class RegularShape{
	constructor(){
		if(this.constructor === RegularShape){
			throw new Error(
				"Object cannot be created from an abstract class RegularShape."
			);
		}

		if(this.setNoSides === undefined){
			throw new Error(
				"Class must implement setNoSides() method."
			);
		}

		if(this.setLength === undefined){
			throw new Error(
				"Class must implement setLength() method."
			);
		}

		if(this.getNoSides === undefined){
			throw new Error(
				"Class must implement getNoSides() method."
			);
		}

		if(this.getLength === undefined){
			throw new Error(
				"Class must implement getLength() method."
			);
		}

		if(this.getPerimeter === undefined){
			throw new Error(
				"Class must implement getPerimeter() method."
			);
		}

		if(this.getArea === undefined){
			throw new Error(
				"Class must implement getArea() method."
			);
		}
	}
}


class Square extends RegularShape{
	#noSides;
	#length;

	constructor(noSides, length){
		super();
		this.#noSides = noSides;
		this.#length = length;
	}

	setNoSides(noSides){
		this.#noSides = noSides;
	}

	setLength(length){
		this.#length = length;
	}

	getNoSides(){
		return this.#noSides;
	}

	getLength(){
		return this.#length;
	}

	getPerimeter(){
		return `The perimeter of the square is ` + (this.#noSides * this.#length);
	}

	getArea(){
		return `The area of the square is ` + (this.#length ** 2);
	}
}

let square1 = new Square();
square1.setNoSides(4);
square1.setLength(65);
console.log(square1.getLength());
console.log(square1.getPerimeter());


class Triangle extends RegularShape{
	#noSides;
	#length;

	constructor(noSides, length){
		super();
		this.#noSides = noSides;
		this.#length = length;
	}

	setNoSides(noSides){
		this.#noSides = noSides;
	}

	setLength(length){
		this.#length = length;
	}

	getNoSides(){
		return this.#noSides;
	}

	getLength(){
		return this.#length;
	}

	getPerimeter(){
		return  `The perimeter of the triangle is ` + (this.#noSides * this.#length);
	}

	getArea(){
		return `The area of the triangle is ` + ((Math.sqrt(3) / 4) * (this.#length ** 2)).toFixed(3);
	}
}
let triangle1 = new Triangle();
triangle1.setNoSides(3);
triangle1.setLength(15);
console.log(triangle1.getLength());
console.log(triangle1.getArea());


/* Activity Part 2 */
class User{
	constructor(){
		if(this.constructor === User){
			throw new Error(
				"Object cannot be created from an abstract class User"
			);
		}

		if(this.login === undefined){
			throw new Error(
				"Class must implement login() method."
			);
		}

		if(this.register === undefined){
			throw new Error(
				"Class must implement register() method."
			);
		}

		if(this.logout === undefined){
			throw new Error(
				"Class must implement logout() method."
			);
		}
	}
}

// regular user
class RegularUser extends User{
	#name;
	#email;
	#password;

	constructor(name, email, password){
		super();
		this.#name = name;
		this.#email = email
		this.#password = password;
	}

	setName(name){
		this.#name = name;
	}

	setEmail(email){
		this.#email = email;
	}

	setPassword(password){
		this.#password = password;
	}

	getName(){
		return this.#name;
	}

	getEmail(){
		return this.#email;
	}

	getPassword(){
		return this.#password;
	}

	login(){
		return `${this.#name} has logged in.`;
	}

	register(){
		return `${this.#name} has registered.`;
	}

	logout(){
		return `${this.#name} has logged out.`;
	}

	browseJobs(){
		return `There are 10 jobs found.`
	}
}

const regUser1 = new RegularUser();
regUser1.setName("Dan");
regUser1.setEmail("dan@mail.com");
regUser1.setPassword("Dan12345");
console.log(regUser1.register());
console.log(regUser1.login());
console.log(regUser1.browseJobs());
console.log(regUser1.logout());


// admin
class Admin extends User{
	#name;
	#email;
	#password;
	#hasAdminExpired;

	constructor(name, email, password, hasAdminExpired){
		super();
		this.#name = name;
		this.#email = email
		this.#password = password;
		this.#hasAdminExpired = hasAdminExpired;
	}

	setName(name){
		this.#name = name;
	}

	setEmail(email){
		this.#email = email;
	}

	setPassword(password){
		this.#password = password;
	}

	setHasAdminExpired(hasAdminExpired){
		this.#hasAdminExpired = hasAdminExpired;
	}

	getName(){
		return this.#name;
	}

	getEmail(){
		return this.#email;
	}

	getPassword(){
		return this.#password;
	}

	getHasAdminExpired(hasAdminExpired){
		return this.#hasAdminExpired;
	}

	login(){
		return `Admin ${this.#name} has logged in.`;
	}

	register(){
		return `Admin ${this.#name} has registered.`;
	}

	logout(){
		return `Admin ${this.#name} has logged out.`;
	}

	postJob(){
		return `Job posting added to site.`;
	}
}


const admin = new Admin();
admin.setName("Joe");
admin.setEmail("admin_joe@mail.com");
admin.setPassword("joe12345");
admin.setHasAdminExpired(false);
console.log(admin.register());
console.log(admin.login());
console.log(admin.getHasAdminExpired());
console.log(admin.postJob());
console.log(admin.logout());