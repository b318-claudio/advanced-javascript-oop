class Request{
	constructor(requesterEmail, content){
		this.requesterEmail = requesterEmail;
		this.content = content;
		this.dateRequested = new Date();
	}
}

class Person{
	constructor(){
		if(this.constructor === Person){
			throw new Error(`Object cannot be created from an abstract class Person`);
		}

		if(this.constructor === Employee){
			if(this.addRequest === undefined){
				throw new Error("Class must implement addRequest() method.");
			}
		}

		if(this.constructor === TeamLead){
			if(this.addMember === undefined){
				throw new Error("Class must implement addMember() method.");
			}

			if(this.checkRequests === undefined){
				throw new Error("Class must implement checkRequests() method.");
			}
		}

		if(this.constructor === Admin){
			if(this.addTeamLead === undefined){
				throw new Error("Class must implement addTeamLead() method.");
			}

			if(this.deactivateTeam === undefined){
				throw new Error("Class must implement deactivateTeam() method.");
			}
		}

		if(this.getFullName === undefined){
			throw new Error("Class must implement getFullName() method.");
		}

		if(this.login === undefined){
			throw new Error("Class must implement login() method.");
		}

		if(this.logout === undefined){
			throw new Error("Class must implement logout() method.");
		}
	}
}


// Employee
class Employee extends Person{

	#firstName;
	#lastName;
	#email;
	#department;
	#isActive
	#requests;

	constructor(firstName, lastName, email, department){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#isActive = true;
		this.#requests = []
	}

	addRequest(message){
		let request = new Request(this.#email, message, this.dateRequested)
		this.#requests.push(request);
		return this.getRequests();
	}

	getRequests(){
		return this.#requests;
	}

	getEmail(){
		return this.#email;
	}

	setEmail(email){
		this.#email = email;
	}

	setIsActive(set){
		this.#isActive = set;
	}

	getIsActive(){
		return this.#isActive;
	}

	setFirstName(firstName){
		this.#firstName = firstName;
	}

	setLastName(lastName){
		this.#lastName = lastName;
	}

	getDepartment(){
		return this.#department;
	}

	setDepartment(department){
		this.#department = department;
	}

	getFullName(){
		return `${this.#firstName} ${this.#lastName}`;
	}

	login(){
		return `${this.#firstName} ${this.#lastName} has logged in`
	}

	logout(){
		return `${this.#firstName} ${this.#lastName} has logged out`
	}
}



// Team Lead
class TeamLead extends Person{

	#firstName;
	#lastName;
	#email;
	#department;
	#isActive
	#members;

	constructor(firstName, lastName, email, department){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#isActive = true;
		this.#members = []
	}

	addMember(employee){
		this.#members.push(employee);
		return this.getMembers();
	}

	checkRequests(email){
		let listRequests;
		listRequests = this.getMembers().find(member =>member.getEmail() === email);
		if(listRequests === undefined){
			console.log(`${email} not found on the list.`);
		} else{
			return listRequests.getRequests();
		}
	}

	getMembers(){
		return this.#members;
	}

	getEmail(){
		return this.#email;
	}

	setEmail(email){
		this.#email = email;
	}

	setIsActive(set){
		this.#isActive = set;
	}

	getIsActive(){
		return this.#isActive;
	}

	setFirstName(firstName){
		this.#firstName = firstName;
	}

	setLastName(lastName){
		this.#lastName = lastName;
	}

	getDepartment(){
		return this.#department;
	}

	setDepartment(department){
		this.#department = department;
	}

	getFullName(){
		return `${this.#firstName} ${this.#lastName}`;
	}

	login(){
		return `${this.#firstName} ${this.#lastName} has logged in`
	}

	logout(){
		return `${this.#firstName} ${this.#lastName} has logged out`
	}
}



// Admin
class Admin extends Person{

	#firstName;
	#lastName;
	#email;
	#department;
	#teamLeads;

	constructor(firstName, lastName, email, department){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#teamLeads = []
	}

	addTeamLead(teamLead){
		this.#teamLeads.push(teamLead);
		return this.getTeamLeads();
	}

	deactivateTeam(email){
		let list;
		list = this.getTeamLeads().find(member =>member.getEmail() === email);
		if(list === undefined){
			console.log(`${email} not found on the list.`);
		} else{
			list.setIsActive(false);
			list.getMembers().forEach(member => member.setIsActive(false))
			return list;
		}
		
	}

	getTeamLeads(){
		return this.#teamLeads;
	}

	getEmail(){
		return this.#email;
	}

	setEmail(email){
		this.#email = email;
	}

	setFirstName(firstName){
		this.#firstName = firstName;
	}

	setLastName(lastName){
		this.#lastName = lastName;
	}

	getDepartment(){
		return this.#department;
	}

	setDepartment(department){
		this.#department = department;
	}

	getFullName(){
		return `${this.#firstName} ${this.#lastName}`;
	}

	login(){
		return `${this.#firstName} ${this.#lastName} has logged in`
	}

	logout(){
		return `${this.#firstName} ${this.#lastName} has logged out`
	}
}




// // Employee Instantiation
// employee1 = new Employee("Ralph", "Claudio", "ralph@mail.com", "developer");
// employee2 = new Employee("Different", "Claudio", "new@mail.com", "developer");

// employee3 = new Employee("New", "employee", "newHR@mail.com", "developer");
// employee4 = new Employee("Old", "employee", "HR@mail.com", "developer");

// // employee1 logging in and adding requests
// console.log(employee1.login());
// employee1.addRequest(`Requesting for a leave`);
// employee1.addRequest(`Requesting for sick leave`);

// // employee1 getting requests
// console.log(employee1.getRequests());

// // employee2 adding request
// employee2.addRequest(`Requesting for a leave`);
// console.log(employee2);


// // employee3 adding request and getting email
// employee3.addRequest(`Requesting for a leave`);
// console.log(employee3.getEmail());

// // employee4 adding requests
// employee4.addRequest(`Requesting for sick leave`);
// employee4.addRequest(`Requesting for a leave`);


// // Team Lead Instantiation
// // teamLead1 instantiate using setters
// teamLead1 = new TeamLead();
// teamLead1.setFirstName("John");
// teamLead1.setLastName("Doe");
// teamLead1.setEmail("john@mail.com");
// teamLead1.setDepartment("developer");
// console.log(teamLead1);

// // teamLead1 adding members and getting teamLead1 department
// teamLead1.addMember(employee1);
// teamLead1.addMember(employee2);
// console.log(teamLead1.getDepartment());

// // teamLead1 checking requests of member with corresponding email
// console.log(teamLead1.checkRequests("ralph@mail.com"));

// // teamLead2 instantiation and adding members and getting full name
// teamLead2 = new TeamLead("Jane", "Smith", "jane@mail.com", "HR");
// console.log(teamLead2);
// teamLead2.addMember(employee3);
// teamLead2.addMember(employee4);
// console.log(teamLead2.getFullName());


// // Admin Instantiation and adding teamLeads and getting team list and deactivating a team and logout
// admin1 = new Admin("J.Y", "Park", "jyp@mail.com", "administrator");
// console.log(admin1);
// admin1.addTeamLead(teamLead1);
// admin1.addTeamLead(teamLead2);
// admin1.deactivateTeam("jane@mail.com");
// console.log(admin1);
// console.log(admin1.getTeamLeads());
// console.log(admin1.logout());

// // employee1 and employee2 checking isActive status
// console.log(`${employee1.getFullName()} status is ${employee1.getIsActive()}`);
// console.log(employee2.getIsActive());

// // teamLead2 and teamLead2 members checking isActive status
// console.log(`${teamLead2.getFullName()} and his/her members status listed below`);
// console.log(`${teamLead2.getFullName()} status: ${teamLead2.getIsActive()}`);
// console.log(`${employee3.getFullName()} status: ${employee3.getIsActive()}`);
// console.log(`${employee4.getFullName()} status: ${employee4.getIsActive()}`);

// Employee Instantiation
employee1 = new Employee("Ralph", "Claudio", "ralph@mail.com", "developer");
employee1.addRequest("Requesting for sick leave");
console.log(employee1.getRequests());

// Team Lead Instantiation
// teamLead1 instantiate using setters
teamLead1 = new TeamLead();
teamLead1.setFirstName("John");
teamLead1.setLastName("Doe");
teamLead1.setEmail("john@mail.com");
teamLead1.setDepartment("developer");
console.log(teamLead1);

// teamLead1 adding members and getting teamLead1 department
teamLead1.addMember(employee1);
console.log(teamLead1.getMembers());
console.log(teamLead1.checkRequests("ralph@mail.com"));

admin1 = new Admin("J.Y", "Park", "jyp@mail.com", "administrator");
admin1.addTeamLead(teamLead1);
console.log(teamLead1.getIsActive());
admin1.deactivateTeam(teamLead1.getEmail());
console.log(teamLead1.getIsActive());


admin1.deactivateTeam("sampleemeil@email.com");